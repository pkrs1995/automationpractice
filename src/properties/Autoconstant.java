package properties;

public interface Autoconstant {
	
	public String GECO_KEY = "webdriver.gecko.driver";

	public String GECO_VALUE = "./driver/geckodriver.exe";

	public String CHROME_KEY = "webdriver.chrome.driver";

	public String CHROME_VALUE = "./driver/chromedriver.exe";
	
	public String OPERA_KEY="webdriver.opera.driver";
	
	public String OPERA_VALUE="./driver/operadriver.exe";

	public String APP_URL = "https://jio.divumapps.com";

	public String EXCEL_PATH = "./dataProvider/TestData.xlsx";
	
//	----------------------------------------------------------------------------------
	
	public String API_BASEURL="https://reqres.in/api/users?page=2";
}
