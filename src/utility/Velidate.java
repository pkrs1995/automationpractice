package utility;

import org.testng.Assert;

public class Velidate {
	
	public static void stringValues(String expected, String actual) {
		Assert.assertEquals(expected, actual);
	}
	
	public static void intValues(int expected, int actual) {
		Assert.assertEquals(expected, actual);
	}
	
	public static void doubleValues(double expected, double actual) {
		Assert.assertEquals(expected, actual);
	}
	
	public static void booleanValues(boolean expected, boolean actual) {
		Assert.assertEquals(expected, actual);
	}
	

}
