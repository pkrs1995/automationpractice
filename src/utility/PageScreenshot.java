package utility;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class PageScreenshot {
	
	public static void getScreenshot (WebDriver driver, String screenshotName) {
		
		try {
			
			TakesScreenshot ts = (TakesScreenshot)driver;
			File src=ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File("./screenshot/"+screenshotName+".png"));
			System.out.println("ScreenShot Taken sucessfully ");
			
			
		} catch (Exception e) {

			System.out.println("Exception while taking Screenshot :" + e);
			
		}
		
	}

}
