package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ReadExcelData {
	
	XSSFWorkbook wb;
	XSSFSheet sheet;
	FileInputStream fis;
	FileOutputStream fos;
	String excelPath;
	File src;
	
	public ReadExcelData(String excelPath, int sheetNumber) {
		
		this.excelPath=excelPath;
		try {
			
			src=new File(excelPath);
			fis = new FileInputStream(src);
			wb=new XSSFWorkbook(fis);
			sheet =wb.getSheetAt(sheetNumber);
		
		} catch (Exception e) {
			
			System.out.println(e.getMessage());
		}
	}
	
	public String getData( int row, int column) {
		
		sheet.getRow(row).getCell(column).setCellType(CellType.STRING);
		String data = sheet.getRow(row).getCell(column).getStringCellValue();
		return data;		
		
	}
	

	public void setData( int row, int column, String str) {
		
		sheet.getRow(row).createCell(column).setCellValue(str);	
		try {	
			
			fos = new FileOutputStream(src);
			wb.write(fos);
			
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		
	}
	
//	public void saveData() {
//		
//		try {	
//			
//		fos = new FileOutputStream(src);
//		wb.write(fos);
//		
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//		}
//	}

}
