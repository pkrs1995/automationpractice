package utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;

import properties.Autoconstant;

public class OpenBrowser {
	
	public static WebDriver chrome() {
		System.setProperty(Autoconstant.CHROME_KEY, Autoconstant.CHROME_VALUE);
		WebDriver driver =new ChromeDriver();
	    driver.manage().window().maximize();
		System.out.println("Chrome Browser Opened");
		return driver;
	}
	
	public static WebDriver firefox() {
		System.setProperty(Autoconstant.GECO_KEY, Autoconstant.GECO_VALUE);
		WebDriver driver =new FirefoxDriver();
	    driver.manage().window().maximize();
		System.out.println("Chrome Browser Opened");
		return driver;
	}
	
	public static WebDriver opera() {
		System.setProperty(Autoconstant.OPERA_KEY, Autoconstant.OPERA_VALUE);
		WebDriver driver =new OperaDriver();
	    driver.manage().window().maximize();
		System.out.println("Chrome Browser Opened");
		return driver;
	}

}
